# TIR Ionic APP

### Installation
The Ionic CLI and cordoba need to be installed in order to run the project, they can be installed by running:
```shell
$ npm install -g ionic cordova
```

Then run npm to get all dependencies needed.
```sh
$ npm install
```

### Serving locally
The app can be served locally on the browser by running:
```sh
$ ionic serve
```

A list with all the command available for the Ionic CLI can be found here: https://ionicframework.com/docs/cli/commands.html