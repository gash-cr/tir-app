export interface Revision {
  createdAt?: Date;
  updatedAt?: Date;
  shippingDetail?: string;
  shipping?: string;
  companyName?: string;
  chassis?: string;
  chassisType?: string;
  truck?: string;
  prefix?: string;
  boatName?: string;
  travelNumber?: number;
  driver?: string;
  guideNumber?: string;
  containerType?: string;
  measure?: string;
  origin?: string;
  destiny?: string;
  genSet?: boolean;
  tiresCondition?: string;
  part?: [{ name: string, in: boolean, out: boolean }];
  observations?: string;
  customsMark1?: string;
  customsMark2?: string;
  customsMark3?: string;
  customsMark4?: string;
  customsMark5?: string;
  customsMark6?: string;
  chassisDetail?: [{ name: string, type: boolean }];
  tire1?: string;
  tire2?: string;
  tire3?: string;
  tire4?: string;
  tire5?: string;
  tire6?: string;
  damages?: [{
    formName: string, damagedContainer: string, damage:
      {
        abbreviation: string,
        name: string,
        image_ref: string
      }, coordinateX: number, coordinateY: number
  }];
 }