import { Revision } from './Revision'

export interface Container {
  createdAt?: Date;
  updatedAt?: Date;
  containerCode: string;
  shipperName: string;
  revision: Revision
}