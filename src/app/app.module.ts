import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { LoginPage } from '../pages/login/login';
import { HttpModule } from '@angular/http';
import { SharedPeviewService } from '../providers/SharedPreviewService/shared-preview-service';
import { TirPage } from '../pages/tir/tir';
import { RevisionPage } from '../pages/tir/revision/revision';
import { TirServiceProvider } from '../providers/tir-service/tir-service';
import { PreviewPage } from '../pages/tir/preview/preview';
import { ViewDamagePage } from '../pages/tir/damage/damagesManagement/viewDamages/viewDamages';
import { DamagesPopover } from '../pages/tir/damage/damagesManagement/damagesPopover/damagesPopover';
import { DamagePage } from '../pages/tir/damage/damage';
import { PendingRevisionsPage } from '../pages/tir/revision/pendingRevision/pendingRevision';
import { FillProviderService } from '../providers/FillProviderService/fill-provider-service';
import { Camera } from '@ionic-native/camera';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    PreviewPage,
    TirPage,
    RevisionPage,
    ViewDamagePage,
    DamagesPopover,
    DamagePage,
    PendingRevisionsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    RevisionPage,
    ViewDamagePage,
    DamagesPopover,
    PreviewPage,
    TirPage,
    RevisionPage,
    DamagePage,
    PendingRevisionsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthServiceProvider,
    SharedPeviewService,
    TirServiceProvider,
    FillProviderService,
    Camera
  ]
})
export class AppModule {}