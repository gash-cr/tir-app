import { Injectable } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { CHASSIS_TYPES, CONTAINER_TYPES, MEASURES, ORIGINS, DESTINATIONS, CHASSIS_DETAILS, TIRES_CONDITIONS, TIR_PARTS } from "../../const/constants";

@Injectable()
export class SharedPeviewService {
  readonly CHASSIS_TYPES = CHASSIS_TYPES;
  readonly CONTAINER_TYPES = CONTAINER_TYPES;
  readonly MEASURES = MEASURES;
  readonly ORIGINS = ORIGINS;
  readonly DESTINATIONS = DESTINATIONS;
  readonly CHASSIS_DETAILS = CHASSIS_DETAILS;
  readonly TIRES_CONDITIONS = TIRES_CONDITIONS;
  readonly PARTS = TIR_PARTS;
  revisionForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.revisionForm = this.formBuilder.group({
      shipperName: new FormControl('Gash'),
      containerCode: new FormControl('', Validators.required),
      shippingDetail: new FormControl(''),
      shipping: new FormControl(''),
      companyName: new FormControl(''),
      truck: new FormControl('', Validators.required),
      prefix: new FormControl(''),
      chassis: new FormControl(''),
      chassisType: new FormControl('GASH'),
      driver: new FormControl(''),
      guideNumber: new FormControl('', Validators.required),
      containerType: new FormControl('Estándar (ST)'),
      customsMark1: new FormControl(''),
      customsMark2: new FormControl(''),
      customsMark3: new FormControl(''),
      customsMark4: new FormControl(''),
      customsMark5: new FormControl(''),
      customsMark6: new FormControl(''),
      measure: new FormControl(''),
      origin: new FormControl('San José'),
      destiny: new FormControl(''),
      boatName: new FormControl(''),
      travelNumber: new FormControl(''),
      genSet: [false],
      chassisDetail: [this.CHASSIS_DETAILS],
      tiresCondition: new FormControl('Nuevas'),
      tire1: new FormControl(''),
      tire2: new FormControl(''),
      tire3: new FormControl(''),
      tire4: new FormControl(''),
      tire5: new FormControl(''),
      tire6: new FormControl(''),
      part: [this.PARTS],
      observations: new FormControl(''),
      damages: new FormControl([]),
      createdAt: new FormControl('')
    });
  }

  cleanDetails() {
    this.revisionForm.reset();
    this.revisionForm.patchValue({ genSet: false });
    this.revisionForm.patchValue({ chassisType: 'GASH' });
    this.revisionForm.patchValue({ containerType: 'Estándar (ST)' });
    this.revisionForm.patchValue({ origin: 'San José' });
    this.revisionForm.patchValue({ tiresCondition: 'Nuevas' });
    this.revisionForm.patchValue({ chassisDetail: this.CHASSIS_DETAILS });
    this.revisionForm.value.chassisDetail.forEach((chassisDetail) => {
      chassisDetail.model = false;
    });
    this.revisionForm.patchValue({ part: this.PARTS });
    this.revisionForm.patchValue({ damages: [] });
    this.revisionForm.value.part.forEach((part) => {
      part.in = false;
      part.out = false;
    });
  }
}