import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { User } from "../../app/models/User";
import { API_URL } from '../../environments/environment';

@Injectable()
export class AuthServiceProvider {

  loginUrl = API_URL + 'auth';
  constructor(private storage: Storage,
              private http: Http) { }

  login(user: User) {
    return this.http.post(`${this.loginUrl}`, user, {});
  }
 
  logout(): void {
    this.storage.remove('token');
    this.storage.remove('user');
  }
 
  authenticated(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.storage.get('currentSession').then((val) => {
        const sessionExists = val ? true : false;
        resolve(sessionExists);
      });
    });
  }
}