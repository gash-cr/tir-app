import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Container } from "../../app/models/Container";
import { API_URL } from '../../environments/environment';

@Injectable()
export class TirServiceProvider {

  containerUrl = API_URL + 'container';

  constructor(private http: Http) {}

  addTir(container: Container) {
    return this.http.post(this.containerUrl, container, {});
  }
}