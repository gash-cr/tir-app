import { Injectable } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { SharedPeviewService } from "../SharedPreviewService/shared-preview-service";

@Injectable()
export class FillProviderService {
  pendingRevisions;
  revisionForm: FormGroup;

  constructor(public formService: SharedPeviewService) {
    this.revisionForm = this.formService.revisionForm;
  }

  getProviderData(revision) {
    Object.keys(revision).map((key) => {
      this.revisionForm.patchValue({ [key]: revision[key] })
    })
  }
}