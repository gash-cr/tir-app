import { Component, ErrorHandler } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { TIR_PARTS } from '../../../const/constants';
import { SharedPeviewService } from '../../../providers/SharedPreviewService/shared-preview-service';
import { Storage } from '@ionic/storage';

declare var epson: any;

@IonicPage()
@Component({
  selector: 'page-preview',
  templateUrl: 'preview.html'
})
export class PreviewPage {
  readonly PARTS = TIR_PARTS;
  company;
  timestamp = Date.now();
  revisionForm;
  containerDamages;
  canPrintReport: boolean;
  public shipperName;
  ipAddress;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formService: SharedPeviewService,
              private alertCtrl: AlertController,
              private storage: Storage) {
    this.revisionForm = formService.revisionForm;
    this.shipperName = navParams.get('shipperName');
    this.canPrintReport = navParams.get('printReport');
    this.containerDamages = this.revisionForm.value.damages;
    this.ipAddress = this.storage.get('ipAddress');
  }

  exitPreview(): void {
    if (this.canPrintReport) {
      this.formService.cleanDetails();
    }
    this.navCtrl.pop();
  }

  setPrinterIpAddress() {
    let alert = this.alertCtrl.create({
      title: 'Configuración de la dirección IP',
      inputs: [
        {
          name: 'ipAddress',
          placeholder: 'dirección IP: 192.168.0.x'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: data => {
            if (data.ipAddress) {
              this.storage.set('ipAddress', data.ipAddress);
            }
          }
        }
      ]
    });
    alert.present();
  }

  displayAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Aceptar',
          role: 'submit',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  printReport() {
    this.storage.get('ipAddress').then(ip => {
      this.ipAddress = ip;
      let ADDRESS = `http://${this.ipAddress}/cgi-bin/epos/service.cgi?devid=local_printer&timeout=60000`;
      let epos = new epson.ePOSPrint(ADDRESS);

      let builder = this.buildMessage(this.revisionForm);
      this.printerStatus(epos, builder).catch(() => ErrorHandler);
    });
  }

  printerStatus(epos, builder) {
    return new Promise((resolve, reject) => {
      epos.onreceive = () => {
        this.displayAlert('Aviso!', 'Agregado a la cola de impresión');
        reject();
      }

      epos.oncoveropen = () => {
        this.displayAlert('Aviso!', 'Impresora abierta');
        reject();
      }

      epos.onerror = () => {
        this.displayAlert('Aviso!', 'Impresora no conectada');
        reject();
      }

      epos.send(builder.toString()).then(() => {
        this.formService.cleanDetails();
        this.navCtrl.pop();
      });
    });
  }

  addTextLine(builder, text, feed, bold) {
    builder.addTextStyle(false, false, bold, builder.COLOR_1);
    feed ? builder.addText(text).addFeed() : builder.addText(text);
  }

  addCustomsMarks(builder) {
    this.addTextLine(builder, 'Marchamo 1: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.customsMark1 || ''), true, false);

    this.addTextLine(builder, 'Marchamo 2: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.customsMark2 || ''), true, false);

    this.addTextLine(builder, 'Marchamo 3: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.customsMark3 || ''), true, false);

    this.addTextLine(builder, 'Marchamo 4: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.customsMark4 || ''), true, false);

    this.addTextLine(builder, 'Marchamo 5: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.customsMark5 || ''), true, false);

    this.addTextLine(builder, 'Marchamo 6: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.customsMark6 || ''), true, false);

  }

  addTires(builder) {
    this.addTextLine(builder, 'M D #1/2 ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.tire1 || ''), true, false);

    this.addTextLine(builder, 'M D #3/4 ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.tire2 || ''), true, false);

    this.addTextLine(builder, 'M D #5/6 ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.tire3 || ''), true, false);

    this.addTextLine(builder, 'M I #1/2 ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.tire4 || ''), true, false);

    this.addTextLine(builder, 'M I #3/4 ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.tire5 || ''), true, false);

    this.addTextLine(builder, 'M I #5/6 ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.tire6 || ''), true, false);
  }

  addTruckDetails(builder) {
    this.addTextLine(builder, 'Naviera: ', false, true);
    this.addTextLine(builder, ((this.revisionForm.value.companyName) ? this.revisionForm.value.companyName : this.revisionForm.value.shipperName), true, false);

    this.addTextLine(builder, 'Medida: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.measure || ''), true, false);

    this.addTextLine(builder, 'Predio Origen: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.origin || ''), true, false);

    this.addTextLine(builder, 'Guia: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.guideNumber || ''), true, false);

    this.addTextLine(builder, 'Predio Destino: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.destiny || ''), true, false);

    this.addTextLine(builder, 'Cabezal: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.truck || ''), true, false);

    this.addTextLine(builder, 'Conductor: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.driver || ''), true, false);

    this.addTextLine(builder, 'Chasis: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.chassis || ''), true, false);

    this.addTextLine(builder, 'Tipo de Chasis: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.chassisType || ''), true, false);
  }

  addContainerDetails(builder) {
    this.addTextLine(builder, 'Fecha y Hora: ', false, true);
    this.addTextLine(builder, this.dateFormatter(new Date(this.revisionForm.value.createdAt)), true, false);

    this.addTextLine(builder, 'Entrada y Salida: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.shippingDetail || ''), true, false);

    this.addTextLine(builder, 'Contenedor: ', false, true);
    this.addTextLine(builder, (this.revisionForm.value.containerCode || ''), true, false);

    this.addTextLine(builder, 'Tipo de Contenedor:', false, true);
    this.addTextLine(builder, (this.revisionForm.value.containerType || ''), true, false);
  }

  builderSetup(builder, context, canvas) {
    builder.addPageBegin();
    builder.addPageArea(0, 0, 300, 300);
    builder.addPagePosition(0, 299);
    builder.addImage(context, 0, 0, canvas.width, canvas.height);
    builder.addPageEnd();
    builder.addFeedLine(2);
    builder.addTextLang('en');
  }

  buildMessage(revision) {
    let builder = new epson.ePOSBuilder();
    let canvas: any = document.getElementById('canvas-report');
    let context = canvas.getContext('2d');
    let logo = document.getElementById('logo');
    context.drawImage(logo, 10, 10);

    this.builderSetup(builder, context, canvas);

    this.addContainerDetails(builder);

    this.addCustomsMarks(builder);

    this.addTruckDetails(builder);

    this.printObservations(builder);

    this.addTires(builder);

    this.printParts(builder, revision);
    builder.addFeedLine(2);

    this.printDamages(builder, revision);
    builder.addCut(builder.CUT_FEED);
    return builder;
  }

  dateFormatter(givenDate) {
    return (givenDate.getDate() + '/' + (givenDate.getMonth() + 1) +
      '/' + givenDate.getFullYear() + ' ' + givenDate.getHours()
      + ':' + givenDate.getMinutes());
  }

  printObservations(builder) {
    this.addTextLine(builder, 'Observaciones:', true, true);
    if (this.revisionForm.value.observations) {
      this.addTextLine(builder, (this.revisionForm.value.observations || ''), false, false);
      builder.addFeedLine(2);
    } else {
      this.addTextLine(builder, 'Sin Observaciones ', true, true);
    }
  }

  printParts(builder, revision) {
    if (revision.value.parts) {
      this.addTextLine(builder, 'Parte   ', false, true);
      this.addTextLine(builder, 'Entrada   ', false, true);
      this.addTextLine(builder, 'Salida   ', true, true);

      if (revision.value.parts.length > 0) {
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        this.revisionForm.value.parts.forEach(part => {
          let partIn = part.in ? 'si' : 'no';
          let partOut = part.out ? 'si' : 'no';
          this.addTextLine(builder, (part.name + ' ' + partIn + ' ' + partOut), false, false);
        });
      } else {
        this.addTextLine(builder, 'Sin Partes E/S ', true, true);
      }
    }
  }

  printDamages(builder, revision) {
    this.addTextLine(builder, 'Daños ', true, true);
    if (revision.value.damages.length > 0) {
      revision.value.damages.forEach(damage => {
        this.addTextLine(builder, ('Parte: ' + (damage.damagedContainer + ', Daño: ' + damage.damage.name || '')), true, true);
      });
    } else {
      this.addTextLine(builder, 'Sin Daños ', true, true);
    }
  }
}