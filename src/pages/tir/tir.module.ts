import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TirPage } from './tir';

@NgModule({
  declarations: [
    TirPage,
  ],
  imports: [
    IonicPageModule.forChild(TirPage),
  ],
})
export class TirPageModule {}