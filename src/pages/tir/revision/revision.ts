import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CHASSIS_TYPES, CONTAINER_TYPES, MEASURES, ORIGINS, DESTINATIONS, CHASSIS_DETAILS, TIRES_CONDITIONS, TIR_PARTS } from '../../../const/constants';
import { SharedPeviewService } from '../../../providers/SharedPreviewService/shared-preview-service';

@IonicPage()
@Component({
  selector: 'page-revision',
  templateUrl: 'revision.html',
})

export class RevisionPage {
  readonly CHASSIS_TYPES = CHASSIS_TYPES;
  readonly CONTAINER_TYPES = CONTAINER_TYPES;
  readonly MEASURES = MEASURES;
  readonly ORIGINS = ORIGINS;
  readonly DESTINATIONS = DESTINATIONS;
  readonly CHASSIS_DETAILS = CHASSIS_DETAILS;
  readonly TIRES_CONDITIONS = TIRES_CONDITIONS;
  readonly PARTS = TIR_PARTS;
  revisionForm;
  shipperName;
  selectAllIn = false;
  selectAllOut = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private sharedPreviewService: SharedPeviewService) {
    this.revisionForm = this.sharedPreviewService.revisionForm;
    this.shipperName = navParams.data;
    this.revisionForm.patchValue({ shipperName: this.shipperName.type });
  }

  selectAllInParts() {
    this.selectAllIn = !this.selectAllIn;
    this.revisionForm.value.part.forEach((part) => {
      part.in = this.selectAllIn;
    });
  }

  selectAllOutParts() {
    this.selectAllOut = !this.selectAllOut;
    this.revisionForm.value.part.forEach((part) => {
      part.out = this.selectAllOut;
    });
  }
}