import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PendingRevisionsPage } from './pendingRevision';

@NgModule({
  declarations: [
    PendingRevisionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PendingRevisionsPage),
  ],
})

export class PendingRevisionsPageModule { }