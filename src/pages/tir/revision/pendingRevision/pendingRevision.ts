import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormGroup } from '@angular/forms';
import { SharedPeviewService } from '../../../../providers/SharedPreviewService/shared-preview-service';
import { FillProviderService } from '../../../../providers/FillProviderService/fill-provider-service';

@IonicPage()
@Component({
  selector: 'page-pending-revisions',
  templateUrl: 'pendingRevision.html',
})
export class PendingRevisionsPage {
  pendingRevisions;
  revisionForm: FormGroup;
  fillDataService;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private storage: Storage,
              public formService: SharedPeviewService,
              public fillData: FillProviderService) {
    this.revisionForm = this.formService.revisionForm;
    this.fillDataService = fillData;
  }

  ionViewDidLoad() {
    this.getPendingRevisions();
  }

  selectRevision(revision) {
    this.fillDataService.getProviderData(revision);
    this.viewCtrl.dismiss(revision);
  }

  closePendingRevisionsModal() {
    this.viewCtrl.dismiss();
  }

  getPendingRevisions() {
    this.storage.get('savedPendingRevisions').then((val) => {
      this.pendingRevisions = val;
    });
  }
}