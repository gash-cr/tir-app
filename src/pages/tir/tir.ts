import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ModalController } from 'ionic-angular';
import { SharedPeviewService } from '../../providers/SharedPreviewService/shared-preview-service';
import { RevisionPage } from './revision/revision';
import { LoginPage } from '../login/login';
import { PreviewPage } from './preview/preview';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { FormGroup, FormControl } from '@angular/forms';
import { TirServiceProvider } from '../../providers/tir-service/tir-service';
import { Container } from '../../app/models/Container';
import { Revision } from '../../app/models/Revision';
import { CHASSIS_TYPES, CONTAINER_TYPES, MEASURES, ORIGINS, DESTINATIONS, CHASSIS_DETAILS, TIRES_CONDITIONS, TIR_PARTS } from '../../const/constants';
import { DamagePage } from './damage/damage';
import { PendingRevisionsPage } from './revision/pendingRevision/pendingRevision';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-tir',
  templateUrl: 'tir.html',
})

export class TirPage {
  readonly CHASSIS_TYPES = CHASSIS_TYPES;
  readonly CONTAINER_TYPES = CONTAINER_TYPES;
  readonly MEASURES = MEASURES;
  readonly ORIGINS = ORIGINS;
  readonly DESTINATIONS = DESTINATIONS;
  readonly CHASSIS_DETAILS = CHASSIS_DETAILS;
  readonly TIRES_CONDITIONS = TIRES_CONDITIONS;
  readonly PARTS = TIR_PARTS;
  tabRevisionPage = RevisionPage;
  tabDamagePage = DamagePage;
  public params = { type: 'Gash' }
  revisionForm: FormGroup;
  loading = false;
  user;
  printReport = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formService: SharedPeviewService,
              private authService: AuthServiceProvider,
              public tirService: TirServiceProvider,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              public modalCtrl: ModalController,
              private storage: Storage) {
    this.revisionForm = this.formService.revisionForm;
    this.storage.get('savedPendingRevisions').then((value) => {
      if (!value) {
        this.storage.set('savedPendingRevisions', []);
      }
    })
    this.storage.get('user').then((val) => {
      this.user = val
    })
  }

  addPending() {
    this.revisionForm.patchValue({ shipperName: this.params.type });
    this.storage.get('savedPendingRevisions').then((storedRevision) => {
      let selectedRev = this.getRevisionForm();
      storedRevision = storedRevision.filter(revision => revision.truck !== selectedRev.truck);
      if (this.revisionForm.value.truck) {
        storedRevision.push(selectedRev);
        this.storage.set('savedPendingRevisions', storedRevision);
        this.presentConfirmAddPending();
      } else {
        this.validatetruckFormFields(this.revisionForm);
      }
    });
  }

  presentPendingRevisionModal() {
    let profileModal = this.modalCtrl.create(PendingRevisionsPage, { enableBackdropDismiss: false });
    profileModal.onDidDismiss(data => {
      if (data) {
        this.params.type = data.shipperName;
      }
    });
    profileModal.present();
  }

  goTologOut() {
    this.authService.logout();
    this.navCtrl.setRoot(LoginPage);
  }

  goToPreview() {
    this.navCtrl.push(PreviewPage, { form: this.revisionForm, shipperName: this.params.type, printReport: this.printReport });
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  validatetruckFormFields(formGroup: FormGroup) {
    const control = formGroup.get('truck');
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    }
  }

  saveRevision() {
    this.revisionForm.patchValue({ shipperName: this.params.type });
    if (this.revisionForm.valid) {
      const revision: Revision = this.getRevisionForm();
      const container: Container =
      {
        containerCode: this.revisionForm.value.containerCode,
        shipperName: this.params.type,
        revision: revision
      };
      this.loading = true;
      if (this.revisionForm.valid) {
        this.presentConfirm(container);
      }
    } else {
      this.validateAllFormFields(this.revisionForm);
    }
  }

  getRevisionForm() {
    this.revisionForm.patchValue({ createdAt: new Date(Date.now()) });
    return {
      createdAt: this.revisionForm.value.createdAt,
      user: this.user,
      shipperName: this.params.type,
      containerCode: this.revisionForm.value.containerCode,
      shippingDetail: this.revisionForm.value.shippingDetail,
      shipping: this.revisionForm.value.shipping,
      companyName: this.revisionForm.value.companyName,
      chassis: this.revisionForm.value.chassis,
      chassisType: this.revisionForm.value.chassisType,
      truck: this.revisionForm.value.truck,
      prefix: this.revisionForm.value.prefix,
      boatName: this.revisionForm.value.boatName,
      travelNumber: this.revisionForm.value.travelNumber,
      driver: this.revisionForm.value.driver,
      guideNumber: this.revisionForm.value.guideNumber,
      containerType: this.revisionForm.value.containerType,
      measure: this.revisionForm.value.measure,
      origin: this.revisionForm.value.origin,
      destiny: this.revisionForm.value.destiny,
      genSet: this.revisionForm.value.genSet,
      chassisDetail: this.revisionForm.value.chassisDetail,
      tiresCondition: this.revisionForm.value.tiresCondition,
      part: this.revisionForm.value.part,
      observations: this.revisionForm.value.observations,
      customsMark1: this.revisionForm.value.customsMark1,
      customsMark2: this.revisionForm.value.customsMark2,
      customsMark3: this.revisionForm.value.customsMark3,
      customsMark4: this.revisionForm.value.customsMark4,
      customsMark5: this.revisionForm.value.customsMark5,
      customsMark6: this.revisionForm.value.customsMark6,
      tire1: this.revisionForm.value.tire1,
      tire2: this.revisionForm.value.tire2,
      tire3: this.revisionForm.value.tire3,
      tire4: this.revisionForm.value.tire4,
      tire5: this.revisionForm.value.tire5,
      tire6: this.revisionForm.value.tire6,
      damages: this.revisionForm.value.damages
    };
  }

  saveSuccess(res) {
    this.loading = false;
    let selectedRev = this.getRevisionForm();
    this.storage.get('savedPendingRevisions').then((val) => {
      val = val.filter(revision => revision.truck != selectedRev.truck);
      this.storage.set('savedPendingRevisions', val);
    });
    if (this.printReport) {
      this.goToPreview();
    } else {
      this.formService.cleanDetails();
    }
  }

  saveError(err) {
    this.loading = false;
    let toast = this.toastCtrl.create({
      message: 'Error al guardar la revisión',
      duration: 3000,
      position: 'top'
    });

    toast.present();
  }

  presentConfirm(container) {
    let alert = this.alertCtrl.create({
      title: 'Aviso',
      message: 'Registrar revisión',
      cssClass: 'alert-custom-css',
      enableBackdropDismiss: false,
      inputs: [
        {
          name: 'printCheckbox',
          type: 'checkbox',
          label: 'Imprimir',
          checked: true
        }
      ],
      buttons: [
        {
          text: 'Guardar',
          role: 'submmit',
          handler: () => {
            this.printReport = alert.data.inputs[0].checked;
            this.tirService.addTir(container)
              .subscribe(res => {
                this.saveSuccess(res);
              },
                err => this.saveError(err));
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  presentConfirmAddPending() {
    let alert = this.alertCtrl.create({
      title: 'Revisión añadida con éxito',
      message: '¿Desea seguir editando?',
      cssClass: 'alert-custom-css',
      buttons: [
        {
          text: 'Sí',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'No',
          role: 'submmit',
          handler: () => {
            this.formService.cleanDetails();
          }
        }
      ]
    });
    alert.present();
  }
}