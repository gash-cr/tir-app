import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { SharedPeviewService } from '../../../providers/SharedPreviewService/shared-preview-service';
import { GASH_DAMAGES, NYK_DAMAGES, EVERGREEN_DAMAGES } from '../../../const/constants';
import { ViewDamagePage } from './damagesManagement/viewDamages/viewDamages';

@IonicPage()
@Component({
  selector: 'page-damage',
  templateUrl: 'damage.html'
})
export class DamagePage implements OnInit {
  revisionForm;
  damages = [];
  shipperName;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private sharedPreviewService: SharedPeviewService,
              private damageModal: ModalController) {
    this.shipperName = navParams.data;
  }

  ngOnInit() {
    this.revisionForm = this.sharedPreviewService.revisionForm;
  }

  loadDamages(shipperName) {
    switch (shipperName.type) {
      case 'Gash':
        this.damages = GASH_DAMAGES;
        break;
      case 'NYK':
        this.damages = NYK_DAMAGES;
        break;
      case 'Evergreen':
        this.damages = EVERGREEN_DAMAGES;
        break;
      default:
        break;
    }
  }

  damageSelection(damagedContainer, imagepath, damagedPartId) {
    this.loadDamages(this.shipperName);
    let selectedDamages = this.revisionForm.value.damages ? this.revisionForm.value.damages : [];
    let currentDamages = selectedDamages.filter(damageSide => damageSide.damagedContainer === damagedContainer);
    let modal = this.damageModal.create(ViewDamagePage, { formName: this.shipperName.type, damagedContainer: damagedContainer, imagepath: imagepath, damagedPartId: damagedPartId,
                                                          formDamages: this.damages, currentDamages: currentDamages }, { enableBackdropDismiss: false });
    modal.present();
    modal.onDidDismiss(data => {
      selectedDamages = selectedDamages.filter(damageSide => damageSide.damagedContainer !== data.damagedContainer);
      if (data.saveParam && data.damagesList.length > 0) {
        Array.prototype.push.apply(selectedDamages, data.damagesList);
      }
      this.revisionForm.patchValue({ damages: selectedDamages });
    })
  }
}