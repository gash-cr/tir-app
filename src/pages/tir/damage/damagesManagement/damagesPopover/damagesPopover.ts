import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AlertController } from 'ionic-angular';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'damages-popover',
  templateUrl: 'damagesPopover.html'
})

export class DamagesPopover {
  selectedDamage;
  damage: any = {
    abbreviation: String,
    name: String,
    image_ref: null
  }

  constructor(public viewController: ViewController,
              private navParams: NavParams,
              private camera: Camera,
              private alertCtrl: AlertController,
              private sanitizer: DomSanitizer) {
    this.selectedDamage = this.navParams.get('selectedDamage');
  }

  takePicture() {
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      quality: 80,
      targetWidth: 500,
      targetHeight: 500,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = imageData;
      this.damage.image_ref = base64Image;
    }, () => {
      let alert = this.alertCtrl.create({
        title: 'Alerta',
        subTitle: 'Error al abrir la cámara',
        buttons: ['Cerrar']
      });
      alert.present();
    });
  }

  close() {
    this.viewController.dismiss(null);
  }

  addDamage() {
    this.damage.name = this.selectedDamage.name;
    this.damage.abbreviation = this.selectedDamage.abbreviation;
    this.viewController.dismiss(this.damage);
  }

  getImgContent(): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(`data:image/jpeg;base64,${this.damage.image_ref}`);
  }

  removeImage() {
    this.damage.image_ref = "";
  }
}