import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewDamagePage } from '../viewDamages/viewDamages';

@NgModule({
  declarations: [
    ViewDamagePage,
  ],
  imports: [
    IonicPageModule.forChild(ViewDamagePage),
  ],
})
export class ViewDamagePageModule {}
