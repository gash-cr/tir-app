import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Content, PopoverController } from 'ionic-angular';
import { DamagesPopover } from '../damagesPopover/damagesPopover';

@IonicPage()
@Component({
  selector: 'page-viewDamage',
  templateUrl: 'viewDamages.html'
})

export class ViewDamagePage {
  damage: string;
  formDamages: any;
  formName: string;
  damagedContainer: string;
  damagedPartId: string;
  damageCount: number;
  damagePopover: any;
  savedDamagesCount: number;
  damagesList = [];
  canvasBackground = new Image();
  selectedDamage;
  timer = 0;
  delay = 250;
  prevent = false;

  @ViewChild('imageCanvas') canvas: ElementRef;
  canvasElement: any;
  context: any;
  @ViewChild(Content) content: Content;
  @ViewChild('fixedContainer') fixedContainer: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private view: ViewController,
              public renderer: Renderer,
              public popoverCtrl: PopoverController) {
    this.damage = navParams.get('damage');
    this.formDamages = navParams.get('formDamages');
    this.formName = navParams.get('formName');
    this.damagedContainer = navParams.get('damagedContainer');
    this.damagedPartId = navParams.get('damagedPartId');
    this.damageCount = 0;
    this.selectedDamage = this.formDamages[0];
    this.canvasBackground.src = navParams.get('imagepath');
    if (navParams.get('currentDamages')) {
      this.damagesList = navParams.get('currentDamages');
    }
  }

  ionViewDidLoad(): void {
    this.canvasElement = this.canvas.nativeElement;
    this.context = this.canvasElement.getContext('2d');
    this.canvasBackground.onload = () => {
      this.canvasElement.width = this.canvasBackground.width;
      this.canvasElement.height = this.canvasBackground.height;
      this.context.drawImage(this.canvasBackground, 0, 0, this.canvasBackground.width, this.canvasBackground.height);
      this.drawAlldamages();
    };

    this.canvasElement.addEventListener("click", (event) => {
      this.timer = setTimeout(() => {
        if (!this.prevent) {
          this.insertMark(event)
        }
        this.prevent = false;
      }, this.delay);
    });
    this.canvasElement.addEventListener("dblclick", (event) => {
      clearTimeout(this.timer);
      this.prevent = true;
      this.insertMark(event);
    });
  }

  insertMark(event) {
    let clickedX = Math.abs(event.offsetX);
    let clickedY = Math.abs(event.offsetY);
    switch (event.type) {
      case 'click':
        this.insertDamageWithoutImage(clickedX, clickedY);
        break;
      case 'dblclick':
        this.initImageDamagePopover(clickedX, clickedY);
        break;
      default:
        break;
    }
  }

  insertDamageWithoutImage(clickedX, clickedY) {
    this.drawDamage(clickedX - 10, clickedY + 10, this.selectedDamage.abbreviation);
    let damagedParts = {
      formName: this.formName, damagedContainer: this.damagedContainer, damagedPartId: this.damagedPartId,
      damage: this.selectedDamage, coordinateX: clickedX - 10, coordinateY: clickedY + 10
    };
    this.drawDamage(damagedParts.coordinateX, damagedParts.coordinateY, damagedParts.damage.abbreviation);
    this.damageInsertion(damagedParts)
  }

  drawDamage(x: number, y: number, text: string): void {
    this.context.font = "40px Arial";
    this.context.fillStyle = "red";
    this.context.fillText(text, x, y);
  }

  deleteDamage(object: any): void {
    this.damagesList.splice(this.damagesList.indexOf(object), 1);
    this.drawAlldamages();
  }

  drawAlldamages(): void {
    this.context.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);
    this.context.drawImage(this.canvasBackground, 0, 0, this.canvasBackground.width, this.canvasBackground.height);
    for (let damage of this.damagesList) {
      this.drawDamage(damage.coordinateX, damage.coordinateY, damage.damage.abbreviation);
    }
  }

  initImageDamagePopover(coordinateX: number, coordinateY: number): void {
    this.damagePopover = this.popoverCtrl.create(DamagesPopover, { selectedDamage: this.selectedDamage },
      { enableBackdropDismiss: false, cssClass: "damage-popover-position" });
    this.damagePopover.present();
    this.damagePopover.onDidDismiss((damage) => {
      if (damage != null) {
        let damagedParts = {
          formName: this.formName, damagedContainer: this.damagedContainer, damagedPartId: this.damagedPartId,
          damage: damage, coordinateX: coordinateX - 10, coordinateY: coordinateY + 10
        };
        this.drawDamage(damagedParts.coordinateX, damagedParts.coordinateY, damagedParts.damage.abbreviation);
        this.damageInsertion(damagedParts);
      }
    });
  }

  damageInsertion(damagedParts) {
    this.damageCount += 1;
    this.damagesList.push(damagedParts);
  }

  saveDamages(saveParam: boolean): void {
    if (!saveParam) {
      this.view.dismiss({ damagedContainer: '', damagesList: this.damagesList, saveParam: false });
    }
    else {
      this.view.dismiss({ damagedContainer: this.damagedContainer, damagesList: this.damagesList, saveParam: true });
    }
  }
}