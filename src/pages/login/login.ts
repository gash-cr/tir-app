import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../app/models/User';
import { ToastController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';
import { TirPage } from '../tir/tir';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public username;
  public password;
  loading = false;
  submitted = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private authService: AuthServiceProvider,
              private toastCtrl: ToastController,
              private storage: Storage) { }

  login() {
    const user: User = { username: this.username, password: this.password };
    this.submitted = true;
    this.loading = true;
    this.authService.login(user)
      .subscribe(res => this.authSuccess(res),
        err => this.authError(err));
  }

  authSuccess(res) {
    this.storage.set("token", JSON.parse(res._body).token);
    this.loading = false;
    this.storage.set('user', this.username);
    this.navCtrl.setRoot(TirPage);
  }

  authError(err) {
    this.loading = false;
    let toast = this.toastCtrl.create({
      message: 'Usuario o contraseña incorrectos.',
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
    });

    toast.present();
  }
}