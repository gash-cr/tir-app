export const TIR_PARTS = [
  { 'name': 'Pulmones', 'in': false, 'out': false },
  { 'name': 'Twist Locks', 'in': false, 'out': false },
  { 'name': 'Manguera', 'in': false, 'out': false },
  { 'name': 'Manilla', 'in': false, 'out': false },
  { 'name': 'Faldones', 'in': false, 'out': false },
  { 'name': 'Alambrados', 'in': false, 'out': false },
  { 'name': 'Patas de Apoyo', 'in': false, 'out': false },
  { 'name': 'Cintas Reflectoras', 'in': false, 'out': false },
  { 'name': 'Cadena', 'in': false, 'out': false },
  { 'name': 'Resortes, etc', 'in': false, 'out': false },
  { 'name': 'Direccionales', 'in': false, 'out': false },
  { 'name': 'Stop', 'in': false, 'out': false },
  { 'name': 'Cola', 'in': false, 'out': false },
  { 'name': 'Luces Laterales', 'in': false, 'out': false },
  { 'name': 'Otros', 'in': false, 'out': false },
  { 'name': 'Cigüeñal', 'in': false, 'out': false }
];

export const CONTAINER_TYPES = [
  { 'name': 'Estándar (ST)' },
  { 'name': 'Refrigerado (RH)' },
  { 'name': 'HIGH-CUBE (HQ)' },
  { 'name': 'OPEN Top-OP' },
  { 'name': 'Flat' },
  { 'name': 'ISO TANK' }
];

export const MEASURES = [
  { 'name': '20' },
  { 'name': '40' },
  { 'name': '45' },
  { 'name': '48' },
  { 'name': '53' }
];

export const ORIGINS = [
  { 'name': 'Limón' },
  { 'name': 'San José' },
  { 'name': 'Caldera' },
  { 'name': 'Nicaragua' }
];

export const DESTINATIONS = [
  { 'name': 'Limón' },
  { 'name': 'San José' },
  { 'name': 'Caldera' },
  { 'name': 'Nicaragua' }
];

export const CHASSIS_DETAILS = [
  { 'name': 'Batería', 'model': false },
  { 'name': 'Marchamo Batería', 'model': false },
  { 'name': 'Alternador', 'model': false },
  { 'name': 'Arrancador', 'model': false },
  { 'name': 'Bomba Eléctrica', 'model': false }
];

export const CHASSIS_TYPES = [
  { 'name': 'GASH' },
  { 'name': 'Traytesa' },
  { 'name': 'Terconsa' },
  { 'name': 'Furgones GASH' },
  { 'name': 'Furgones Terconsa' },
  { 'name': 'Particular' }
];

export const TIRES_CONDITIONS = [
  { 'name': 'Nuevas' },
  { 'name': 'Lisas' },
  { 'name': 'Recauchadas' }
];

export const GASH_DAMAGES =[
  {'abbreviation': 'C', 'name': 'Cortadura'},
  {'abbreviation': 'R', 'name': 'Rayón'},
  {'abbreviation': 'H', 'name': 'Hueco'},
  {'abbreviation': 'A', 'name': 'Abolladura'},
  {'abbreviation': 'Q', 'name': 'Quebradura'},
  {'abbreviation': 'F', 'name': 'Faltante'},
  {'abbreviation': 'P', 'name': 'Parche'},
  {'abbreviation': 'G', 'name': 'Gotera'}
];

export const NYK_DAMAGES = [
  {'abbreviation': 'C', 'name': 'Cortadura'},
  {'abbreviation': 'B', 'name': 'Bruise'},
  {'abbreviation': 'H', 'name': 'Hueco'},
  {'abbreviation': 'D', 'name': 'Abolladura'},
  {'abbreviation': 'BR', 'name': 'Quebradura'},
  {'abbreviation': 'M', 'name': 'Faltante'}
];

export const EVERGREEN_DAMAGES = [
  {'abbreviation': 'C', 'name': 'Cortadura'},
  {'abbreviation': 'SC', 'name': 'Rayón'},
  {'abbreviation': 'H', 'name': 'Hueco'},
  {'abbreviation': 'D', 'name': 'Abolladura'},
  {'abbreviation': 'BR', 'name': 'Quebradura'},
  {'abbreviation': 'M', 'name': 'Faltante'},
  {'abbreviation': 'B', 'name': 'Pandeo'},
  {'abbreviation': 'BN', 'name': 'Doblado'},
  {'abbreviation': 'CO', 'name': 'Corrosión'},
  {'abbreviation': 'CR', 'name': 'Grieta'},
  {'abbreviation': 'S', 'name': 'Manchado'}
];